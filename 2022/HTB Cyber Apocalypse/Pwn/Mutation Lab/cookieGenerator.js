var cookieSession = require('cookie-session')
var express = require('express')

var app = express()

var sessionToken = "5921719c3037662e94250307ec5ed1db";
app.use(cookieSession({
  name: 'session',
  keys: [sessionToken]
}))

app.get('/', function (req, res, next) {
  req.session.username = 'admin'
  res.send()
})

app.listen(3000)