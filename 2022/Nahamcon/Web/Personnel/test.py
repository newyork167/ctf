import re

flag = open("flag.txt").read()
users = open("users.txt").read()

users += flag

name = '|'
setting = 82

results = re.findall(r"[A-Z][a-z]*?" + name + r"[a-z]*?\n", users, setting)
print(results)

results = [x.strip() for x in results if x or len(x) > 1]

print(results)