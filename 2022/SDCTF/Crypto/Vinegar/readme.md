# Vinegar

![](info.png)

Yelling something about "Vinega" makes me think Vigenere so let's try that. Throwing `wbeyrjgewcfroggpesremvxgvefyrcmnnymxhdacgnnrwprhxpuyyaupbmskjrxfopr` into [dcode](https://www.dcode.fr/vigenere-cipher) we get the following:

![](dcode.png)

So we get some words out of the starting key `UNKNO`, so setting that up as a partial key and just moving forward with more `?` in the partial decoder we eventually get to:

![](dcode2.png)

`Flag: sdctf{couldntuseleetstringsinthisonesadlybutwemadeitextralongtocompensate}`