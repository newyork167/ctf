# Susan Album Party

![](info.png)

We are given a stub of a binary blob. Running `file` we get:

```
original/stub: JPEG image data, progressive, precision 8, 240x320, components 3
```

So it's a jpeg! Let's give it a better filename and see what's inside.

![](stub.jpeg)

So, we see the first part of the flag! Initially I ran through `stegsolve`. `foremost` and `binwalk` and didn't really get anything out of any of them. So, looking again at the first part of the flag I decided to see how many instances of `\xff\xd8` are in the stub file.

```
xxd -p stub.jpeg | tr " " "\n" | grep -c "ffd8"
4
```

So we can see there are at most 4 jpeg files embedded in this one. So I whipped up `solution.py` to extract them out and dump them to their own jpeg files. The final files are:

![](0.jpg)

![](1.jpg)

![](2.jpg)

and I'm guessing just a jibberish file for `3.jpg`. 

Flag: `sdctf{FFD8_th3n_S0ME_s7uff_FFD9}`
