from asyncore import write
import binascii

def write_jpg(i, image):
    try:
        with open(f'{i}.jpg', 'wb+') as jpeg_file:
            jpeg_file.write(binascii.unhexlify(image))
    except Exception as ex:
        print(f"Could not write {i}: {ex}")

def get_all_images(image):
    return [b'ffd8' + x for x in image.split(b'ffd8')][1:]


my_file = "stub.jpeg"
with open(my_file, 'rb') as file_t:
    blob_data = binascii.hexlify(file_t.read())
    images = get_all_images(blob_data)

    for i, image in enumerate(images):
        if len(image) % 2 != 0:
            image += b'0'
        write_jpg(i, image)
        print(image[:10])
        

# sdctf{FFD8_th3n_S0ME_s7uff_FFD9}