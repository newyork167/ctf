# Google Ransom

![](info.png)

The link takes us to a google doc:

![](doc.png)

Google docs have a lot of metadata that is easily parsable with a tool called [xeuledoc](https://github.com/Malfrats/xeuledoc). In this case, we get the following information:

```bash
xeuledoc https://docs.google.com/document/d/1MbY-aT4WY6jcfTugUEpLTjPQyIL9pnZgX_jP8d8G2Uo/edit\?usp\=sharing
Twitter : @MalfratsInd
Github : https://github.com/Malfrats/xeuledoc

Document ID : 1MbY-aT4WY6jcfTugUEpLTjPQyIL9pnZgX_jP8d8G2Uo

[+] Creation date : 2022/01/05 22:58:38 (UTC)
[+] Last edit date : 2022/01/05 23:08:11 (UTC)

Public permissions :
- reader

[+] Owner found !

Name : Amy SDCTF
Email : amy.sdctf@gmail.com
Google ID : 13481488189780380748
```

Now to send an email! Jumped over to my favorite email site [sharklasers](sharklasers.com) and sent out a very demanding email to Amy. Had to wait a bit but finally got an email back!

![](email.png)

`Flag: sdctf{0p3n_S0uRCE_1S_aMaz1NG}`
