# Oil Spill

![](info.png)

```bash
gef➤  checksec
[+] checksec for 'OilSpill'
Canary                        : ✓
NX                            : ✓
PIE                           : ✘
Fortify                       : ✘
RelRO                         : ✘
```

```bash
gef➤  disas main
Dump of assembler code for function main:
   0x000000000040068a <+0>:	push   rbp
   0x000000000040068b <+1>:	mov    rbp,rsp
   0x000000000040068e <+4>:	sub    rsp,0x150
   0x0000000000400695 <+11>:	mov    DWORD PTR [rbp-0x144],edi
   0x000000000040069b <+17>:	mov    QWORD PTR [rbp-0x150],rsi
   0x00000000004006a2 <+24>:	mov    rax,QWORD PTR fs:0x28
   0x00000000004006ab <+33>:	mov    QWORD PTR [rbp-0x8],rax
   0x00000000004006af <+37>:	xor    eax,eax
   0x00000000004006b1 <+39>:	lea    rax,[rbp-0x140]
   0x00000000004006b8 <+46>:	lea    r8,[rip+0xffffffffffffffb8]        # 0x400677 <temp>
   0x00000000004006bf <+53>:	mov    rcx,rax
   0x00000000004006c2 <+56>:	mov    rax,QWORD PTR [rip+0x200557]        # 0x600c20
   0x00000000004006c9 <+63>:	mov    rdx,rax
   0x00000000004006cc <+66>:	mov    rax,QWORD PTR [rip+0x200545]        # 0x600c18
   0x00000000004006d3 <+73>:	mov    rsi,rax
   0x00000000004006d6 <+76>:	lea    rdi,[rip+0x141]        # 0x40081e
   0x00000000004006dd <+83>:	mov    eax,0x0
   0x00000000004006e2 <+88>:	call   0x400588 <printf@plt>
   0x00000000004006e7 <+93>:	lea    rdi,[rip+0x142]        # 0x400830
   0x00000000004006ee <+100>:	call   0x400580 <puts@plt>
   0x00000000004006f3 <+105>:	lea    rdi,[rip+0x17e]        # 0x400878
   0x00000000004006fa <+112>:	call   0x400580 <puts@plt>
   0x00000000004006ff <+117>:	mov    rax,QWORD PTR [rip+0x20059a]        # 0x600ca0 <stdout@@GLIBC_2.2.5>
   0x0000000000400706 <+124>:	mov    rdi,rax
   0x0000000000400709 <+127>:	call   0x400570 <fflush@plt>
   0x000000000040070e <+132>:	mov    rdx,QWORD PTR [rip+0x20059b]        # 0x600cb0 <stdin@@GLIBC_2.2.5>
   0x0000000000400715 <+139>:	lea    rax,[rbp-0x140]
   0x000000000040071c <+146>:	mov    esi,0x12c
   0x0000000000400721 <+151>:	mov    rdi,rax
   0x0000000000400724 <+154>:	call   0x400560 <fgets@plt>
   0x0000000000400729 <+159>:	lea    rax,[rbp-0x140]
   0x0000000000400730 <+166>:	mov    rdi,rax
   0x0000000000400733 <+169>:	mov    eax,0x0
   0x0000000000400738 <+174>:	call   0x400588 <printf@plt>
   0x000000000040073d <+179>:	lea    rdi,[rip+0x20053c]        # 0x600c80 <x>
   0x0000000000400744 <+186>:	call   0x400580 <puts@plt>
   0x0000000000400749 <+191>:	mov    rax,QWORD PTR [rip+0x200550]        # 0x600ca0 <stdout@@GLIBC_2.2.5>
   0x0000000000400750 <+198>:	mov    rdi,rax
   0x0000000000400753 <+201>:	call   0x400570 <fflush@plt>
   0x0000000000400758 <+206>:	mov    eax,0x0
   0x000000000040075d <+211>:	mov    rcx,QWORD PTR [rbp-0x8]
   0x0000000000400761 <+215>:	xor    rcx,QWORD PTR fs:0x28
   0x000000000040076a <+224>:	je     0x400771 <main+231>
   0x000000000040076c <+226>:	call   0x400550 <__stack_chk_fail@plt>
   0x0000000000400771 <+231>:	leave
   0x0000000000400772 <+232>:	ret
   ```

   