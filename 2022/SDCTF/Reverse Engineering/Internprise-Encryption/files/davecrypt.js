/**
 * Dave's Awesome Verygood Encryption scheme (DAVEs)
 * I, the magnificent cryptographer Dave, have invented an encryption scheme
 * that I myself have proven impossible to crack.
 * If I can't crack it, no one can!
 */
 function encrypt(s) {
	let encrypted = [];
	for (let i = 0; i < s.length; i++) { // Loop over all characters
		let x = (s[i].charCodeAt(0x0) + i * 0xf) % 0x80; 
		// x is (s[i].charCodeAt(0) + i * 15) % 128
		
		x += i > 0x0 ? encrypted[i - 0x1].charCodeAt(0) % 128 : 0xd; 
		// if x is first character -> x += 13
		// if x is secon character -> x += encrypted[i-1].charCodeAt(0)

		x ^= 0x555;
		// x is x xor 1365

		x = ((x ^ ~0x0) >>> 0x0) & 0xff;
		// x is (x xor -1) & 255

		x -= (Math.random() * 0x5) & 0xb9 & 0x46;
		// x -= (breakMe * 5) & 185 & 70

		x = ~(x ^ (0x2cd + ((i ^ 0x44) % 0x2 === 0) ? 0x3 : 0x0));
		// 
		
		x = ((x >> 0x1f) + x) ^ (x >> 0x1f);
		// 
		
		x |= ((Date.now() % 0x3e8) / (0x4d2 - 0xea)) | (i % 0x1);
		// 
		
		encrypted.push(String.fromCharCode(x));
	}
	return encrypted.join("");
}