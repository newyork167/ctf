from pwn import *

"""
b'Show me you are worthy and solve for x! You have one second.\n'
b'1216574887684406 * 5435560960733322 = \n'
1216574887684406 * 5435560960733322
6612766965305883184176263976732
b'> You have proven yourself to be capable of taking on the final task. Decrypt this and the flag shall be yours!\n'
b'5c38671b57b967d1e546335854ebed7068c105ee61739cc2795dfa8378079135\n'
b'> You are wrong. 2 guesses remain.\n'
b'> You are wrong. 1 guess remains.\n'
b'> You are wrong. 0 guesses remain.\n'

"""

url = "dctf-chall-just-take-your-time.westeurope.azurecontainer.io"
port = 9999

conn = remote(url, port)

l = conn.recvline()
print(l)
l = conn.recvline()
print(l)

# Solve first challenge
n = str(l)[2:].split(' = ')[0]
print(n)
r = eval(n)
print(r)
conn.send(f'{r}\r\n')
l = conn.recvline()

print(l)
print(conn.recvline())
conn.send(f't\r\n')
print(conn.recvline())
conn.send(f't\r\n')
print(conn.recvline())
conn.send(f't\r\n')
print(conn.recvline())
conn.close()