# [Static ain't always noise](https://play.picoctf.org/practice/challenge/163)

Author: SYREAL

Points: 20

## Description

Can you look at the data in this binary: [static](https://mercury.picoctf.net/static/bc72945175d643626d6ea9a689672dbd/static)? This [BASH script](https://mercury.picoctf.net/static/bc72945175d643626d6ea9a689672dbd/ltdis.sh) might help!

## Solution

<details>
  <summary>Flag spoiler warning</summary>

  Strings is your friend...
</details>