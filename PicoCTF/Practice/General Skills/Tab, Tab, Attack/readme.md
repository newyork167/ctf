# [Tab, Tab, Attack](https://play.picoctf.org/practice/challenge/176)

Author: SYREAL

Points: 20

## Description

Using tabcomplete in the Terminal will add years to your life, esp. when dealing with long rambling directory structures and filenames: [Addadshashanammu.zip](https://mercury.picoctf.net/static/659efd595171e4c40378be6a2e9b7298/Addadshashanammu.zip)

## Solution

<details>
  <summary>Flag spoiler warning</summary>

  Tab complete will certainly save years, as does strings!
</details>