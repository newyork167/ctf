# [Transformation](https://play.picoctf.org/practice/challenge/104)

Author: MADSTACKS

Points: 20

## Categories:

- Reverse Engineering

## Description

I wonder what this really is... [enc](https://mercury.picoctf.net/static/2b4cea9b07db22bf4f933fddd1b8caa9/enc) `''.join([chr((ord(flag[i]) << 8) + ord(flag[i + 1])) for i in range(0, len(flag), 2)])`

## Solution

<details>
  <summary>Flag spoiler warning</summary>

  Basic gist is that it goes in batches of two along the flag, converts both ascii characters to their ordinal values, bit shifts the first by 8 bits and stuffs both into a 16 bit binary number. 
  
  So, take the number and convert to binary string. Then break down into two 8 bit numbers and voila you have your original flag ordinals!
</details>