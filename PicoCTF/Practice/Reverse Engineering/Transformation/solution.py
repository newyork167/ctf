# for i in range flag, increment 2
#   chr((ord(flag[i]) << 8) + ord(flag[i + 1]))

solution = ''
print(10 << 8)
with open('enc', 'r') as enc_file:
    encrypted = "".join(enc_file.readlines())
    for i in encrypted:
        str_bin_j = bin(ord(i))[2:].zfill(16)

        first_num = int(str_bin_j[:8], 2)
        second_num = int(str_bin_j[-8:], 2)
        solution += chr(first_num) + chr(second_num)

print(solution)